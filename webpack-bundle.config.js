const path = require('path');
const webpack = require('webpack');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const BundleBuddyWebpackPlugin = require('bundle-buddy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ParallelUglifyPlugin = require('webpack-parallel-uglify-plugin');

module.exports = function (options = {}) {
  const isProd = options.isProd || false;
  const devtool = options.isProd ? false : 'source-map';
  const mode = options.isProd ? 'production' : 'development';
  const outputPath = path.resolve(__dirname, 'dist');

  const entry = {
    common: [
      'react',
      'react-dom',
      'react-router',
      'react-router-dom',
      'redux',
      'react-redux',
      'redux-thunk',
      'redux-form',
    ],
    app: './src/index.js',
  };

  const module = {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        use: [
          'react-hot-loader/webpack',
          'babel-loader',
          'eslint-loader',
        ],
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: isProd ? MiniCssExtractPlugin.loader : 'style-loader',
            options: {
              hmr: !isProd,
            },
          },
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'image/svg+xml',
          },
        },
      },
      {
        test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: 'url-loader?limit=10000&minetype=application/font-woff&name=./[hash].[ext]',
          options: {
            limit: 10000,
            mimetype: 'application/font-woff',
          },
        },
      },
      {
        test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: 'url-loader?limit=10000&minetype=application/font-woff&name=./[hash].[ext]',
          options: {
            limit: 10000,
            mimetype: 'application/font-woff',
          },
        },
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: 'url-loader?limit=10000&minetype=application/font-woff&name=./[hash].[ext]',
          options: {
            limit: 10000,
            mimetype: 'application/octet-stream',
          },
        },
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        use: 'file-loader',
      },
    ],
  };

  const plugins = [
    new webpack.DefinePlugin({
      ENVIRONMENT: JSON.stringify(mode),
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(isProd ? 'production' : 'development'),
      },
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: isProd,
      debug: !isProd,
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      allChunks: true,
    }),
    new CleanWebpackPlugin({
      verbose: true,
    }),
    new CopyWebpackPlugin([
      {
        from: './public/images',
        to: 'images',
      },
      {
        from: './public/mocks/',
        to: 'mocks',
      },
      {
        from: './public/messages',
        to: 'messages',
      },
    ], {
      copyUnmodified: true,
    }),
    new HtmlWebpackPlugin({
      inject: false,
      minify: {
        collapseWhitespace: true,
        preserveLineBreaks: false,
      },
      chunks: ['common', 'app'],
      filename: 'index.html',
      template: `./src/templates/index.${mode}.ejs`,
      title: 'Rivambrosa',
      favicon: './public/images/favicon.ico',

    }),
  ];

  if (options.bundleBuddy) {
    plugins.push(new BundleBuddyWebpackPlugin());
  }

  if (options.analyzeBundle) {
    plugins.push(new BundleAnalyzerPlugin());
  }

  if (isProd) {
    plugins.push(new ParallelUglifyPlugin({}));
  }

  const optimization = {
    splitChunks: {
      cacheGroups: {
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true,
        },
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          name: 'common',
          priority: 0,
          chunks: 'all',
          reuseExistingChunk: true,
        },
      },
    },
  };

  const output = {
    path: outputPath,
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
    sourceMapFilename: '[file].map',
    publicPath: isProd ? '/myaccountx/' : '',
  };

  return {
    entry,
    devtool,
    mode,
    module,
    optimization,
    output,
    plugins,
  };
};
