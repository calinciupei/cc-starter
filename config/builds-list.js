const buildsList = [
  {
    brand: 'ribambrosa',
  },
];

const getBuilds = (config) => {
  config = config || {};
  const brand = new RegExp(config.brand || '.*');

  return buildsList.filter(build => build.brand.match(brand));
};

module.exports = {
  buildsList,
  getBuilds,
};

