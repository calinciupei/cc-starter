import {DEPENDENCY_DONE} from './initApp.constants';

export const getDependencySuccess = () => (dispatch) => (
  dispatch({
    type: DEPENDENCY_DONE,
  })
);
