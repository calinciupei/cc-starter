import React, {Component} from 'react';
import {connect} from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {Card, CardMedia, CardContent} from '@material-ui/core';

export class HomePage extends Component {
  state = {
    dependenciesLoaded: false,
  };

  componentWillMount() {
    this.setState({dependenciesLoaded: true});
  }

  render() {
    return (
      <Grid
        container
        spacing={0}
        alignItems="center"
        justify="center"
        style={{
          minHeight: '100vh',
          textAlign: 'center',
        }}
      >
        <Grid item xs={12} sm={6}>
          <Card>
            <CardMedia
              component="img"
              alt="Rivambrosa under construction"
              image="/images/logo.png"
              style={{
                width: 'auto',
                height: 'auto',
                textAlign: 'center',
                display: 'inline-block',
                padding: '20px',
              }}
            />
            <CardContent>
              <Typography variant="h6" color="textSecondary" gutterBottom>Under Construction...</Typography>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    );
  }
}

HomePage.propTypes = {};

export default connect(state => ({
  homePage: state.homePage,
}))(HomePage);
