const bundleConfig = require('./webpack-bundle.config');
const buildsList = require('./config/builds-list');

// Common webpack configs and variables
const commonConfig = {
  build: process.env.BUILD_NUMBER || '_BUILD_',
  brand: process.env.BRAND,
  isProd: process.env.NODE_ENV === 'production',
};

const buildAll = (config) => {
  const configs = [];

  for (const build of buildsList.getBuilds(config)) {
    const buildId = `${build.brand}`;

    // eslint-disable-next-line
    console.log(`Generating config for ${build.brand}`);

    // Merge the common and given build
    const mergedConfig = Object.assign({}, commonConfig, build, {
      isProd: config.isProd,
      analyzeBundle: config.analyzeBundle,
      bundleBuddy: config.bundleBuddy,
    });

    // Push the configs for webpack
    configs.push(
      {
        config: bundleConfig(mergedConfig),
        buildId: buildId,
      }
    );
  }

  return configs;
};

module.exports = function (config = {}) {
  return buildAll(config);
};
